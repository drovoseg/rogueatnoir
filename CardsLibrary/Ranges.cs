﻿using System;
using System.Resources;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Reflection;
using System.Windows.Forms;


namespace CardsLibrary
{
	public enum Ranges 
	{ 
		Ace = 1, 
		Two, 
		Three, 
		Four, 
		Five, 
		Six, 
		Seven, 
		Eight, 
		Nine, 
		Ten, 
		Jack, 
		Quin, 
		King 
	};
}
