﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;


namespace CardsLibrary
{


    /// <summary>
    /// Класс ячейки, в которой размещается стопка
    /// </summary> 
    public class Cell
    {
        float m_X;
        /// <summary>
        /// X-координата, по которой располагается ячейка
        /// </summary>
        public float X
        {
            get { return m_X; }
            set
            {
                m_X = value;
                if (DockPile != null) DockPile.X = m_X;
            }
        }

        float m_Y;
        /// <summary>
        /// Y-координата, по которой располагается ячейка
        /// </summary>
        public float Y 
        {
            get { return m_Y; }
            set
            {
                m_Y = value;
                if (DockPile != null) DockPile.Y = m_Y;
            }
        }
        
        /// <summary>
        /// Ширина ячейки
        /// </summary>
        public float Width { get; set; }

        /// <summary>
        /// Высота ячейки
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// Размер ячейки
        /// </summary>
        public SizeF Size { get { return new SizeF(Width, Height); } }

        /// <summary>
        /// Положение ячейки на экране 
        /// </summary>
        public PointF Location { get { return new PointF(X, Y); } }

        CellType m_celltype;

        /// <summary>
        /// Характерный тип ячейки
        /// </summary>
        public CellType Celltype
        {
            get { return m_celltype; }
            set { m_celltype = value; }
        }
        Image m_cellimage;

        /// <summary>
        /// Стопка, которая помещена в ячейку
        /// </summary>
        public CardPile DockPile { get; set; }

        /// <summary>
        /// Создает новый объект типа Cell c указанным типом и координатами расположения
        /// </summary>
        /// <param name="celltype">Тип создаваемой ячейки</param>
        /// <param name="x">Координата ячейки по оси X</param>
        /// <param name="y">Координата ячейки по оси Y</param>
        public Cell(CellType celltype, float x, float y)
        {
            X = x; Y = y;
            m_celltype = celltype;

            switch (m_celltype)
            {
                case CellType.KingCell: m_cellimage = CardResourses.king_cell; break;
                case CellType.AceCell: m_cellimage = CardResourses.ace_cell; break;
                case CellType.SetSuiteCell: m_cellimage = CardResourses.setsuite_cell; break;
                case CellType.EmptyCell: m_cellimage = CardResourses.empty_cell; break;
            }
            Width = m_cellimage.Width;
            Height = m_cellimage.Height;

            DockPile = null;
        }

        /// <summary>
        /// Создает новый объект типа Cell c указанным типом, координатами расположения, а также размерами
        /// </summary>
        /// <param name="celltype">Тип создаваемой ячейки</param>
        /// <param name="x">Координата ячейки по оси X</param>
        /// <param name="y">Координата ячейки по оси Y</param>
        /// <param name="width">Ширина создаваемой ячейки</param>
        /// <param name="height">Высота создаваемой ячейки</param>
        public Cell(CellType celltype, float x, float y, float width, float height)
        {
            X = x; Y = y; 
            Width = width;
            Height = height;
            m_celltype = celltype;

            switch (m_celltype)
            {
                case CellType.KingCell: m_cellimage = CardResourses.king_cell; break;
                case CellType.AceCell: m_cellimage = CardResourses.ace_cell; break;
                case CellType.SetSuiteCell: m_cellimage = CardResourses.setsuite_cell; break;
                case CellType.EmptyCell: m_cellimage = CardResourses.empty_cell; break;
            }

            DockPile = null;
        }

        /// <summary>
        /// Отрсовывает ячейку на указанном эл-те управления, используя передаваемый контекст вывода
        /// </summary>
        /// <param name="sender">Элемент управелния, на котором будет отображаться ячейка</param>
        /// <param name="g">Контекст вывода, с использованием которого производится отрисовка</param>
        public void Draw(Control sender, Graphics g)
        {
            RectangleF DrawRect = new RectangleF(Location, Size);
            switch (m_celltype)
            {
                case CellType.KingCell:
                    g.DrawImage(CardResourses.king_cell, DrawRect);
                    break;
                case CellType.AceCell:
                    g.DrawImage(CardResourses.ace_cell, DrawRect);
                    break;
                case CellType.SetSuiteCell:
                    g.DrawImage(CardResourses.setsuite_cell, DrawRect);
                    break;
                case CellType.EmptyCell:
                    g.DrawImage(CardResourses.empty_cell, DrawRect);
                    break;
            }
        }
    }
}
