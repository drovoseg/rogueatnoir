﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Collections;
using System.Collections.ObjectModel;


namespace CardsLibrary
{   /// <summary>
    /// Класс "стопка карт", содержащий коллекцию объектов 
    /// класса Card и методы по работе с ней
    /// </summary>
    public class CardPile : IList<Card>
    {
        List<Card> m_Cards { get; set; }

        float m_X;
        /// <summary>
        /// Координата X, по которой располагается стопка на экране
        /// </summary>
        public float X 
        {
            get { return m_X; }
            set
            {                
                m_X = value;

                if (Count == 0) return;

                m_Cards[0].X = m_X;
                for (int i = 1; i < m_Cards.Count; i++)
                {
                    float k;

                    if (!m_Cards[i - 1].CardBack
                        && (DockCell == null || DockCell.Celltype == CellType.KingCell)) 
                        k = 4.5f;
                    else k = 1;

                    m_Cards[i].X = m_Cards[i - 1].X + k * Dx;
                }
            }
        }

        float m_Y;
        /// <summary>
        /// Координата Y, по которой располагается стопка на экране
        /// </summary>
        public float Y
        {
            get { return m_Y; }
            set
            {                
                m_Y = value;

                if (Count == 0) return;

                m_Cards[0].Y = m_Y;
                for (int i = 1; i < m_Cards.Count; i++)
                {
                    float k;

                    if (!m_Cards[i - 1].CardBack)
                        k = 4.5f;
                    else k = 1;

                    m_Cards[i].Y = m_Cards[i - 1].Y + k * Dy;
                }
            }
        }

        /// <summary>
        /// Координата верхнего левого угла стопки карт
        /// </summary>
        public PointF Location { get { return new PointF(X, Y); } }

        /// <summary>
        /// Растояние по оси X между картами в стопке
        /// </summary>
        public float Dx { get; set; }

        /// <summary>
        /// Растояние по оси Y между картами в стопке
        /// </summary>
        public float Dy { get; set; }
        
        /// <summary>
        /// Ширина отдельной карты в стопке
        /// </summary>
        public float CardWidth 
        { 
            get 
            {
                if (m_DockCell != null)
                    return m_DockCell.Width;
                else if (m_DockCell == null && m_Cards.Count > 0)
                    return m_Cards[0].Width;
                else return 0;
            } 
        }

        /// <summary>
        /// Высота отдельной карты в стопке
        /// </summary>
        public float CardHeight
        {
            get
            {
                if (m_DockCell != null)
                    return m_DockCell.Height;
                else if (m_DockCell == null && m_Cards.Count > 0)
                    return m_Cards[0].Height;
                else return 0;
            }
        }

        /// <summary>
        /// Высота области, которую занимают все карты в стопке
        /// </summary>
        public float PileHeight
        {
            get
            {
                Card lastcard = m_Cards.LastOrDefault();

                if (lastcard == null)
                    return CardHeight;
                else
                    return lastcard.Height + Math.Abs(lastcard.Y - Y);
            }
        }

        /// <summary>
        /// Ширина области, которую занимают все карты в стопке
        /// </summary>
        public float PileWidth
        {
            get
            {
                Card lastcard = m_Cards.LastOrDefault();

                if (lastcard == null)
                    return CardWidth;
                else
                    return lastcard.Width + Math.Abs(lastcard.X - X);
            }
        }

        /// <summary>
        /// Зазмер стопки карт
        /// </summary>
        public SizeF Size { get { return new SizeF(PileWidth, PileHeight); } }

        Cell m_DockCell;
        /// <summary>
        /// Ячейка, к которой привязана стопка
        /// </summary>
        public Cell DockCell 
        {
            get
            {
                return m_DockCell;
            }
            set
            {
                m_DockCell = value;
                if (m_DockCell != null) foreach (Card card in m_Cards) 
                {
                    card.Width = m_DockCell.Width;
                    card.Height = m_DockCell.Height;
                }
            }
        }

        /// <summary>
        /// Количество карт в стопке
        /// </summary>
        public int Count { get { return m_Cards.Count; } }

        /// <summary>
        /// Возвращает или задает элемент по указанному индексу
        /// </summary>
        /// <param name="index">Отсчитываемый от нуля индекс элемента, который требуется получить или задать</param>
        /// <returns>Элемент с указанным индексом</returns>
        public Card this[int index] 
        {
            get { return m_Cards[index]; }
            set { m_Cards[index] = value; }
        }
        bool m_IsReadOnly = true;
        /// <summary>
        /// Переменная, оставленная для совместимости. Не используется!
        /// </summary>
        public bool IsReadOnly { get { return m_IsReadOnly; } }

        /// <summary>
        /// Возвращает прямоуголльную область, которую занимает стопка 
        /// </summary>
        public RectangleF LocationRect
        {
            get
            {
                return new RectangleF(X, Y, PileWidth, PileHeight);
            }
        }

        /// <summary>
        /// Конструктор по-умолчанию, создающий пустую стопку с расположением и дистанцией между картами по-умолчанию
        /// </summary>
        public CardPile()
        {
            m_Cards = new List<Card>();
            m_X = m_Y = 0;
            Dx = 0;
            Dy = 0;
            DockCell = null;
        }

        /// <summary>
        /// Создает новый объект "стопка" с указанным координатами
        /// </summary>
        /// <param name="x">Координата X, по которой располагается стопка на экране</param>
        /// <param name="y">Координата Y, по которой располагается стопка на экране</param>
        public CardPile(float x, float y)
        {
            m_Cards = new List<Card>();
            Dx = 0;
            Dy = 0;
            m_X = x;
            m_Y = y;
            DockCell = null;
        }

        /// <summary>
        /// Создает новый объект "стопка" с указанным координатами, содержащий последовательность карт, передаваемых в качестве параметра
        /// </summary>
        /// <param name="cards">Коллекция карт, помещаемая в стопку</param>
        /// <param name="x">Координата X, по которой располагается стопка на экране</param>
        /// <param name="y">Координата Y, по которой располагается стопка на экране</param>
        public CardPile(List<Card> cards, float x, float y)
        {
            m_Cards = new List<Card>();
            m_X = x;
            m_Y = y;
            
            m_Cards.AddRange(cards);
            for (int i = 0; i < m_Cards.Count; i++)
            {
                m_Cards[i].X = x;
                m_Cards[i].Y = y;
                m_Cards[i].OwnerPile = this;
            }
            DockCell = null;
        }
        
        /// <summary>
        /// Создает пустой объект "стопка" с указанным координатами и дистанцией между картами в стопке
        /// </summary>
        /// <param name="x">Координата X, по которой располагается стопка на экране</param>
        /// <param name="y">Координата Y, по которой располагается стопка на экране</param>
        /// <param name="dx">Растояние по оси X между картами в стопке</param>
        /// <param name="dy">Растояние по оси Y между картами в стопке</param>
        public CardPile(float x, float y, float dx, float dy)
        {
            m_Cards = new List<Card>();
            m_X = x;
            m_Y = y;
            Dx = dx;
            Dy = dy;

            m_Cards = new List<Card>();

            DockCell = null;
        }

        /// <summary>
        /// Создает новый объект "стопка" с указанным координатамии дистанцией между картами в 
        /// стопке, которые передаются в качестве параметра
        /// </summary>
        /// <param name="cards">Коллекция карт, помещаемая в стопку</param>
        /// <param name="x">Координата X, по которой располагается стопка на экране</param>
        /// <param name="y">Координата Y, по которой располагается стопка на экране</param>
        /// <param name="dx">Растояние по оси X между картами в стопке</param>
        /// <param name="dy">Растояние по оси Y между картами в стопке</param>
        public CardPile(List<Card> cards, float x, float y, float dx, float dy)
        {
            m_Cards = new List<Card>();
            m_X = x;
            m_Y = y;
            Dx = dx;
            Dy = dy;
            
            m_Cards.AddRange(cards);
            for (int i = 0; i < m_Cards.Count; i++)
            {
                m_Cards[i].X = m_X + i * dx;
                m_Cards[i].Y = m_Y + i * dy;
                m_Cards[i].OwnerPile = this;
            }
            if (m_Cards.Count > 0) m_Cards[m_Cards.Count - 1].CardBack = true;
            DockCell = null;
        }

        #region Реализация интерфейсов

        /// <summary>
        /// Определяет, входит ли элемент в состав CardsLibrary.CardPile
        /// </summary>
        /// <param name="card">Объект, который требуется найти в CardsLibrary.CardPile Допускается значение null для ссылочных типов.</param>
        /// <returns>true, если элемент item найден в списке CardsLibrary.CardPile, в противном случае — false</returns>
        public bool Contains(Card card)
        {
            return m_Cards.Contains(card);
        }

        public void Clear()
        {
            m_Cards.Clear();
        }

        /// <summary> 
        /// Сводка:
        ///     Осуществляет поиск указанного объекта и возвращает отсчитываемый от нуля
        ///     индекс первого вхождения, найденного в пределах всего списка System.Collections.Generic.List<T>.
        /// </summary>
        /// <param name="card">Карта которую требуется найти в CardsLibrary.CardPile. Допускается значение null для ссылочных типов</param>
        /// <returns>Отсчитываемый от нуля индекс первого вхождения элемента card в пределах всей коллекции CardsLibrary.CardPile, если элемент найден; в противном случае — значение –1</returns>
        public int IndexOf(Card card)
        {
            return m_Cards.IndexOf(card);
        }

        public int IndexOf(Card card, int index)
        {
            return m_Cards.IndexOf(card, index);
        }

        public int IndexOf(Card card, int index, int count)
        {
            return m_Cards.IndexOf(card, index, count);
        }

        public void Insert(int index, Card card)
        {
            m_Cards.Insert(index, card);
        }

        public void RemoveAt(int index)
        {
            m_Cards[index] = null;
            m_Cards.RemoveAt(index);
        }

        public void Add(Card card)
        {
            Card lastcard = m_Cards.LastOrDefault();
            float k = 1;

            if (lastcard == null)
            {
                card.X = X; 
                card.Y = Y;
            }
            else
            {
                if (!lastcard.CardBack
                    && (DockCell == null || DockCell.Celltype == CellType.KingCell))
                    k = 4;

                card.X = lastcard.X + k * Dx;
                card.Y = lastcard.Y + k * Dy;
            }
            m_Cards.Add(card);

            card.OwnerPile = this;
        }

        public void CopyTo(Card[] array)
        {
            m_Cards.CopyTo(array);
        }

        public void CopyTo(Card[] array, int array_index)
        {
            m_Cards.CopyTo(array, array_index);
        }

        public void CopyTo(int index, Card[] array, int array_index, int count)
        {
            m_Cards.CopyTo(index, array, array_index, count);
        }
                
        public bool Remove(Card card)
        {
            card.OwnerPile = null;
            return m_Cards.Remove(card);            
        }

        #region Члены IEnumerable<Card>

        IEnumerator<Card> IEnumerable<Card>.GetEnumerator()
        {
            return m_Cards.GetEnumerator();            
        }

        #endregion

        #region Члены IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
        	return ((IEnumerable<Card>)this).GetEnumerator();
        }

        #endregion

        #endregion        

        public Card MoveCard(CardPile destpile)
        {
            Card movecard;
            float k;

            if (destpile.Count > 0 && !destpile[destpile.Count - 1].CardBack) 
                k = 4;
            else k = 1;

            if (this.m_Cards.Count == 0) return null;
            movecard = this.m_Cards[this.m_Cards.Count - 1];
            destpile.m_Cards.Add(movecard);
            this.m_Cards.Remove(movecard);

            if (destpile.Count == 1)
            {
                movecard.X = destpile.X;
                movecard.Y = destpile.Y;
            }
            else
            {                
                movecard.X = destpile[destpile.Count - 2].X + k * destpile.Dx;
                movecard.Y = destpile[destpile.Count - 2].Y + k * destpile.Dy;
            }
            movecard.OwnerPile = destpile;
            
            return movecard;
        }

        public CardPile SelectPile(int index)
        {
            CardPile newpile = new CardPile(m_Cards[index].X, m_Cards[index].Y);

            newpile.DockCell = null;
            newpile.Dx = this.Dx;
            newpile.Dy = this.Dy;            

            while (index < Count)
            {
                newpile.Insert(0,m_Cards[m_Cards.Count - 1]);
                m_Cards[m_Cards.Count - 1].OwnerPile = newpile;
                this.Remove(m_Cards[m_Cards.Count - 1]);
            }

            if (newpile.Count > 0) return newpile;
            else return null;
        }

        public void PutPile(CardPile putpile)
        {
            Card lastcard = m_Cards.LastOrDefault();
            float k = 1;

            if (lastcard == null)
            {
                putpile.X = X;
                putpile.Y = Y;
            }
            else
            {
                if (!lastcard.CardBack 
                    && (DockCell == null || DockCell.Celltype == CellType.KingCell)) 
                    k = 4;

                putpile.X = lastcard.X + k * Dx;
                putpile.Y = lastcard.Y + k * Dy;
            }
            m_Cards.AddRange(putpile);

            foreach (Card card in putpile) card.OwnerPile = this;
            putpile.DockCell = this.DockCell;
        }

        public void Draw(Control sender, int step, Graphics g)
        {
            if (m_Cards.Count != 0)
            {
                for (int i = 0; i < m_Cards.Count; i += step)
                    m_Cards[i].Draw(sender, g);
            }
            else if (DockCell != null) DockCell.Draw(sender, g);
        }

        public void Draw(Control sender, int step, Graphics g, Card[] hidecards)
        {            
            foreach (Card card in hidecards)
                if (card.OwnerPile != this)
                    throw new InvalidOperationException("Некоторые карты не содержатся в стопке");

            if (m_Cards.Count != 0)
            {
                for (int i = 0; i < m_Cards.Count; i += step)
                    if (!hidecards.Contains(m_Cards[i])) 
                        m_Cards[i].Draw(sender, g);
            }
            else if (DockCell != null) DockCell.Draw(sender, g);
        }
    }
}
