﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;


namespace CardsLibrary
{
	/// <summary>
	/// Перечисление, описывающе тип формируемой ячейки
	/// </summary> 
	public enum CellType { KingCell, AceCell, SetSuiteCell, EmptyCell };
}
