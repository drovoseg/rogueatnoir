﻿using System;
using System.Resources;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Reflection;
using System.Windows.Forms;


namespace CardsLibrary
{
    public class Card
    {
        Suits m_Suite;
        public Suits Suite { get { return m_Suite; } }

        Ranges m_Range;
        public Ranges Range { get { return m_Range; } }
        Image m_CardImage;

        public float X { get; set; }
        public float Y { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public SizeF Size { get { return new SizeF(Width, Height); } }
        public PointF Location { get { return new PointF(X, Y); } }

        bool m_CardBack;
        public bool CardBack
        {
            get { return m_CardBack; }
            set
            {
                m_CardBack = value;
                if (!m_CardBack) m_CardImage = SelectFrontImage();
                else m_CardImage = CardResourses.cardback_mx25;
            }
        }

        public CardPile OwnerPile { get; set; }

        private Image SelectFrontImage()
        {
            switch (Suite)
            {
                case Suits.Clubs:
                    switch (Range)
                    {
                        case Ranges.Ace: return CardResourses.ace_clubs;
                        case Ranges.Two: return CardResourses._2_clubs;
                        case Ranges.Three: return CardResourses._3_clubs;
                        case Ranges.Four: return CardResourses._4_clubs;
                        case Ranges.Five: return CardResourses._5_clubs;
                        case Ranges.Six: return CardResourses._6_clubs;
                        case Ranges.Seven: return CardResourses._7_clubs;
                        case Ranges.Eight: return CardResourses._8_clubs;
                        case Ranges.Nine: return CardResourses._9_clubs;
                        case Ranges.Ten: return CardResourses._10_clubs;
                        case Ranges.Jack: return CardResourses.jack_clubs;
                        case Ranges.Quin: return CardResourses.queen_clubs;
                        case Ranges.King: return CardResourses.king_clubs;
                        default: return null;
                    }

                case Suits.Diamonds:
                    switch (Range)
                    {
                        case Ranges.Ace: return CardResourses.ace_diamonds;
                        case Ranges.Two: return CardResourses._2_diamonds;
                        case Ranges.Three: return CardResourses._3_diamonds;
                        case Ranges.Four: return CardResourses._4_diamonds;
                        case Ranges.Five: return CardResourses._5_diamonds;
                        case Ranges.Six: return CardResourses._6_diamonds;
                        case Ranges.Seven: return CardResourses._7_diamonds;
                        case Ranges.Eight: return CardResourses._8_diamonds;
                        case Ranges.Nine: return CardResourses._9_diamonds;
                        case Ranges.Ten: return CardResourses._10_diamonds;
                        case Ranges.Jack: return CardResourses.jack_diamonds;
                        case Ranges.Quin: return CardResourses.queen_diamonds;
                        case Ranges.King: return CardResourses.king_diamonds;
                        default: return null;
                    }

                case Suits.Hearts:
                    switch (Range)
                    {
                        case Ranges.Ace: return CardResourses.ace_hearts;
                        case Ranges.Two: return CardResourses._2_hearts;
                        case Ranges.Three: return CardResourses._3_hearts;
                        case Ranges.Four: return CardResourses._4_hearts;
                        case Ranges.Five: return CardResourses._5_hearts;
                        case Ranges.Six: return CardResourses._6_hearts;
                        case Ranges.Seven: return CardResourses._7_hearts;
                        case Ranges.Eight: return CardResourses._8_hearts;
                        case Ranges.Nine: return CardResourses._9_hearts;
                        case Ranges.Ten: return CardResourses._10_hearts;
                        case Ranges.Jack: return CardResourses.jack_hearts;
                        case Ranges.Quin: return CardResourses.queen_hearts;
                        case Ranges.King: return CardResourses.king_hearts;
                        default: return null;
                    }

                case Suits.Spades:
                    switch (Range)
                    {
                        case Ranges.Ace: return CardResourses.ace_spades;
                        case Ranges.Two: return CardResourses._2_spades;
                        case Ranges.Three: return CardResourses._3_spades;
                        case Ranges.Four: return CardResourses._4_spades;
                        case Ranges.Five: return CardResourses._5_spades;
                        case Ranges.Six: return CardResourses._6_spades;
                        case Ranges.Seven: return CardResourses._7_spades;
                        case Ranges.Eight: return CardResourses._8_spades;
                        case Ranges.Nine: return CardResourses._9_spades;
                        case Ranges.Ten: return CardResourses._10_spades;
                        case Ranges.Jack: return CardResourses.jack_spades;
                        case Ranges.Quin: return CardResourses.queen_spades;
                        case Ranges.King: return CardResourses.king_spades;
                        default: return null;
                    }

                default: return null;
            }
        }

        public Card(Suits suit, Ranges range)
        {
            m_Suite = suit;
            m_Range = range;
            X = 0; 
            Y = 0; 
            m_CardImage = CardResourses.cardback_mx25;
            m_CardBack = true;
            Width = m_CardImage.Width;
            Height = m_CardImage.Height;                        
        }

        public Card(Suits suit, Ranges range, float x, float y, float width, float height)
        {
            m_Suite = suit;
            m_Range = range;
            X = x; 
            Y = y; 
            Width = width;
            Height = height;
            m_CardImage = CardResourses.cardback_mx25;
            CardBack = true;            
            OwnerPile = null;            
        }

        public Card(Suits suit, Ranges range, float x, float y)
        {
            m_Suite = suit;
            m_Range = range;
            X = x; 
            Y = y; 
            m_CardImage = CardResourses.cardback_mx25;
            CardBack = true;
            Width = m_CardImage.Width;
            Height = m_CardImage.Height;            
            OwnerPile = null;            
        }

        public override string ToString()
        {
            return Enum.GetName(typeof(Ranges), Range) + " " + Enum.GetName(typeof(Suits), Suite);
        }

        public bool IsCardPoint(float x, float y)
        {
            if (X <= x && Y <= y && x <= X + Width && y <= Y + Height)
                return true;
            else
                return false;
        }

        public bool IsOtherColorCard(Card card)
        {
            if ((this.Suite == Suits.Diamonds || this.Suite == Suits.Hearts)
                && (card.Suite == Suits.Clubs || card.Suite == Suits.Spades))
                return true;

            if ((card.Suite == Suits.Diamonds || card.Suite == Suits.Hearts)
                && (this.Suite == Suits.Clubs || this.Suite == Suits.Spades))
                return true;

            return false;
        }

        public static explicit operator CardPile(Card card)
        {
            CardPile ownpile = card.OwnerPile;
            CardPile pile = new CardPile(new Card[] { card }.ToList(), card.X, card.Y);
            card.OwnerPile = ownpile;
            return pile;
        }

        public void Draw(Control sender, Graphics g)
        {            
            g.DrawImage(m_CardImage, new RectangleF(Location, Size));
        }
    }
}
