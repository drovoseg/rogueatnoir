﻿namespace RogueAtNoir
{
    partial class RulesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.TextOfRules = new System.Windows.Forms.RichTextBox();
        	this.Picture = new System.Windows.Forms.PictureBox();
        	((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// TextOfRules
        	// 
        	this.TextOfRules.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.TextOfRules.Location = new System.Drawing.Point(289, 12);
        	this.TextOfRules.Name = "TextOfRules";
        	this.TextOfRules.ReadOnly = true;
        	this.TextOfRules.Size = new System.Drawing.Size(306, 246);
        	this.TextOfRules.TabIndex = 0;
        	this.TextOfRules.TabStop = false;
        	this.TextOfRules.Text = "";
        	// 
        	// Picture
        	// 
        	this.Picture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.Picture.Image = global::RogueAtNoir.Properties.Resources.kniga;
        	this.Picture.Location = new System.Drawing.Point(12, 12);
        	this.Picture.Name = "Picture";
        	this.Picture.Size = new System.Drawing.Size(267, 246);
        	this.Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
        	this.Picture.TabIndex = 1;
        	this.Picture.TabStop = false;
        	// 
        	// RulesForm
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(607, 273);
        	this.Controls.Add(this.Picture);
        	this.Controls.Add(this.TextOfRules);
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
        	this.MaximizeBox = false;
        	this.MinimizeBox = false;
        	this.Name = "RulesForm";
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        	this.Text = "Правила";
        	((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.RichTextBox TextOfRules;
        private System.Windows.Forms.PictureBox Picture;
    }
}