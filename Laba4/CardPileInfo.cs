﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CardsLibrary;

namespace RogueAtNoir
{
    public struct CardPileInfo
    {
        public CardPile SourcePile;
        public bool LastCardBack;
        public CardPile[] DestPiles;
        public CardPile[] MovePiles;

        public CardPileInfo(CardPile sourcepile, CardPile[] destpiles, CardPile[] movepiles)
        {
            SourcePile = sourcepile;
            if (sourcepile.LastOrDefault() != default(Card))
                LastCardBack = sourcepile.LastOrDefault().CardBack;
            else
                LastCardBack = false;

            DestPiles = destpiles;
            MovePiles = movepiles;
        }
    }
}
