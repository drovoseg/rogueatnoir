﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CardsLibrary;

namespace RogueAtNoir
{
    public partial class MainForm : Form
    {
        const float depthx = 15;
        const float depthy = 8;
        const uint number_steps = 5;

        float width;
        float height;
        int Counter = 0;
        List<CardPileInfo> UserActionSequence;
        int CurUserAction = -1;

        Image DoubleBuffer;
        bool NewGameIsBegin = false;
        CardPile cardbatch;
        Cell cardbatchcell;

        CardPile[] soursepiles = new CardPile[10];
        Cell[] soursecells = new Cell[10];

        CardPile[] first_dest_piles = new CardPile[4];
        Cell[] first_dest_cells = new Cell[4];

        CardPile[] second_dest_piles = new CardPile[4];
        Cell[] second_dest_cells = new Cell[4];

        bool dragging;
        CardPile SelPile = null;
        Point StartDragPoint;
        CardPile StartPile;
        float startX, startY;        
        int black_samecolor_limit = 2;
        int red_samecolor_limit = 2;
        int black_altcolor_limit = 2;
        int red_altcolor_limit = 2;

        int seconds;

        public MainForm()
        {
            InitializeComponent();
            Icon = Properties.Resources.Cardsico;
            
            StartPile = null;
            UserActionSequence = new List<CardPileInfo>();
        }

        string NameCards(int n)
        {
            string s = n.ToString() + ' ';
            int nMod10 = n % 10;
            if ((n >= 11 && n <= 19) 
                || nMod10 == 0
                || nMod10 >= 5) 
                
                return s + "карт";

            else if (nMod10 == 1) return s + "карта";

            else return s + "карты";
        }

        List<Card> GenerateCardSet(ushort number_cardbatch)
        {
            int i = 0;
            Card newcard;
            Card[] cardset = new Card[number_cardbatch * 52];
            Random rnd = new Random();

            for (int n = 0; n < number_cardbatch; n++)
            {
                foreach (Suits suit in Enum.GetValues(typeof(Suits)))
                    foreach (Ranges range in Enum.GetValues(typeof(Ranges)))
                    {
                        newcard = new Card(suit, range);
                        do
                        {
                            i = rnd.Next(cardset.Length);
                        } while (cardset[i] != null);

                        cardset[i] = newcard;
                    }
            }
            return cardset.ToList();
        }

        void ClearCardImage(Card card, Control owner, Graphics g)
        {
            Graphics BufGraph;

            BufGraph = Graphics.FromImage(DoubleBuffer);

            BufGraph.FillRectangle(new SolidBrush(owner.BackColor),
                                   new RectangleF(card.Location, card.Size));            
            
            if (card.OwnerPile == null)
                DrawPiles(ClientArea, new RectangleF(card.Location, card.Size),BufGraph, null);
            else
                DrawPiles(ClientArea, new RectangleF(card.Location, card.Size), BufGraph, card);

            g.DrawImage(DoubleBuffer, card.X, card.Y,
                        new RectangleF(card.Location, card.Size),
                        GraphicsUnit.Pixel);
            GC.Collect();
        }

        void ClearPileImage(CardPile pile, Control owner, Graphics g)
        {
            Graphics BufGraph;
            RectangleF drawrect = new RectangleF(pile.LocationRect.X, 
                                               pile.LocationRect.Y,
                                               pile.LocationRect.Width,
                                               pile.LocationRect.Height);

            BufGraph = Graphics.FromImage(DoubleBuffer);
            BufGraph.FillRectangle(new SolidBrush(owner.BackColor), drawrect);
            DrawPiles(ClientArea, pile.LocationRect, BufGraph, null);

            g.DrawImage(DoubleBuffer, pile.X, pile.Y, drawrect,
                        GraphicsUnit.Pixel);
            GC.Collect();
        }

        void DrawPiles(Control sender, RectangleF drawrect, Graphics g, Card hidecard)
        {
            if (hidecard == null)
            {
                if (drawrect.IntersectsWith(cardbatch.LocationRect))
                    cardbatch.Draw(sender, 20, g);

                foreach (CardPile pile in first_dest_piles)
                {
                    if (drawrect.IntersectsWith(pile.LocationRect))
                        pile.Draw(sender, 1, g);
                }

                foreach (CardPile pile in second_dest_piles)
                {
                    if (drawrect.IntersectsWith(pile.LocationRect))
                        pile.Draw(sender, 1, g);
                }

                foreach (CardPile pile in soursepiles)
                {
                    if (drawrect.IntersectsWith(pile.LocationRect))
                        pile.Draw(sender, 1, g);
                }
            }
            else
            {
                if (drawrect.IntersectsWith(cardbatch.LocationRect))
                {
                    if (hidecard.OwnerPile != cardbatch) cardbatch.Draw(sender, 20, g);
                    else
                        cardbatch.Draw(sender, 20, g, new Card[] { hidecard });
                }

                foreach (CardPile pile in first_dest_piles)
                {
                    if (drawrect.IntersectsWith(pile.LocationRect))
                    {
                        if (hidecard.OwnerPile != pile)
                            pile.Draw(sender, 1, g);
                        else
                            pile.Draw(sender, 1, g, new Card[] { hidecard });
                    }
                }

                foreach (CardPile pile in second_dest_piles)
                {
                    if (drawrect.IntersectsWith(pile.LocationRect))
                    {
                        if (hidecard.OwnerPile != pile)
                            pile.Draw(sender, 1, g);
                        else
                            pile.Draw(sender, 1, g, new Card[] { hidecard });
                    }

                }

                foreach (CardPile pile in soursepiles)
                {
                    if (drawrect.IntersectsWith(pile.LocationRect))
                    {
                        if (hidecard.OwnerPile != pile)
                            pile.Draw(sender, 1, g);
                        else
                            pile.Draw(sender, 1, g, new Card[] { hidecard });
                    }

                }
            }
        }

        void SpreadTheCards(CardPile cardbatch, CardPile[] piles, bool openallcards)
        {            
            Card movecard = null;
            float dx, dy;
            CardPile[] DestPiles = null, MoveCards = null;

            if (openallcards)
            {
                if (piles.Length < cardbatch.Count)
                {
                    DestPiles = new CardPile[piles.Length];
                    MoveCards = new CardPile[piles.Length];
                }
                else
                {
                    DestPiles = new CardPile[cardbatch.Count];
                    MoveCards = new CardPile[cardbatch.Count];
                }
            }
            
            for (int i = 0; i < piles.Length && cardbatch.Count > 0; i++)
            {                
                dx = (piles[i].X - cardbatch.Last().X) / number_steps;
                dy = (piles[i].Y - cardbatch.Last().Y) / number_steps;

                movecard = cardbatch.LastOrDefault();
                if (movecard == default(Card)) return;
                bool result = cardbatch.Remove(movecard);
                
                float x0 = movecard.X;
                float y0 = movecard.Y;
                
                for (int j = 1; j < number_steps; j++)
                {
                    ClearCardImage(movecard, ClientArea, Graphics.FromHwnd(ClientArea.Handle));
                    movecard.X = x0 + dx * j;
                    movecard.Y = y0 + dy * j;
                    movecard.Draw(ClientArea, Graphics.FromHwnd(ClientArea.Handle));
                    System.Threading.Thread.Sleep(5);
                }
                ClearCardImage(movecard, ClientArea, Graphics.FromHwnd(ClientArea.Handle));
                piles[i].Add(movecard);
                if (openallcards)
                {
                    movecard.CardBack = false;
                    DestPiles[i] = piles[i];
                    MoveCards[i] = (CardPile)movecard;
                }
                movecard.Draw(ClientArea, Graphics.FromHwnd(ClientArea.Handle));
                System.Threading.Thread.Sleep(2);
            }
            ClearCardImage(movecard, ClientArea, Graphics.FromHwnd(ClientArea.Handle));
            movecard.CardBack = false;
            movecard.Draw(ClientArea, Graphics.FromHwnd(ClientArea.Handle));

            if (openallcards) FixedUzerAction(cardbatch, DestPiles, MoveCards);
        }

        Card GetItemAt(float X, float Y)
        {
            foreach (CardPile pile in soursepiles)
            {
                for (int i = pile.Count - 1; i >= 0; i--)
                {
                    Card card = pile[i];
                    if (card.IsCardPoint(X, Y)) return card;
                }
            }
            
            if (cardbatch.LastOrDefault() != default(Card) 
                && cardbatch.LastOrDefault().IsCardPoint(X, Y))
                return cardbatch.LastOrDefault();
            
            return null;
        }

        CardPile GetItemAt(CardPile sourcepile)
        {
            RectangleF sourcer = new RectangleF(sourcepile.X, 
                                                sourcepile.Y,
                                                sourcepile.PileWidth,
                                                sourcepile.PileHeight);

            foreach (CardPile destpile in first_dest_piles)
            {
                RectangleF destr = new RectangleF(destpile.X, 
                                                  destpile.Y,
                                                  destpile.PileWidth,
                                                  destpile.PileHeight);
                
                RectangleF intersect = RectangleF.Intersect(destr, sourcer);
               
                if (intersect.Width * intersect.Height >
                    destpile.DockCell.Width * destpile.DockCell.Height / 2)
                    return destpile;
            }

            foreach (CardPile destpile in soursepiles)
            {
                RectangleF destr = new RectangleF(destpile.X, 
                                                  destpile.Y,
                                                  destpile.PileWidth,
                                                  destpile.PileHeight);
                
                RectangleF intersect = RectangleF.Intersect(destr, sourcer);

                if (intersect.Width * intersect.Height > 
                    destpile.DockCell.Width * destpile.DockCell.Height / 2)
                    return destpile;
            }

            return null;
        }

        private void ClientArea_MouseClick(object sender, MouseEventArgs e)
        {
            Card card;

            if (NewGameIsBegin && !dragging)
            {
                card = GetItemAt(e.X, e.Y);

                if (card == null || e.Button != MouseButtons.Left || e.Clicks > 1) return;

                if (card.OwnerPile == cardbatch)
                {
                    SpreadTheCards(cardbatch, soursepiles, true);
                    AdaptScrolling();
                }
            }
        }

        private void ClientArea_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Graphics g;
            CardPile DestPile = null, OwnerPile = null;
            Card card;
            Card lastcard = null;
            bool put_possible = false;
            float dx, dy;

            if (NewGameIsBegin)
            {
                card = GetItemAt(e.X, e.Y);

                if (card == null || e.Button != MouseButtons.Left || card.CardBack) return;

                if (soursepiles.Contains(card.OwnerPile))
                {
                    OwnerPile = card.OwnerPile;

                    for (int i = OwnerPile.IndexOf(card); i < OwnerPile.Count; i++)
                        if (OwnerPile[i].CardBack) return;

                    for (int i = OwnerPile.Count - 2; i >= OwnerPile.IndexOf(card); i--)
                    {
                        bool isothercolor = OwnerPile[i].IsOtherColorCard(OwnerPile[i + 1]);
                        if (((int)OwnerPile[i].Range) - ((int)OwnerPile[i + 1].Range) != 1
                            || !isothercolor)
                            return;
                    }

                    SelPile = OwnerPile.SelectPile(OwnerPile.IndexOf(card));

                    foreach (CardPile pile in first_dest_piles)
                    {
                        if (pile.Count == 0 && SelPile.Count == 1
                            && SelPile[0].Range == Ranges.Ace)
                        {
                            if (SelPile[0].Suite == Suits.Clubs || SelPile[0].Suite == Suits.Spades)
                            {
                                if (black_samecolor_limit > 0)
                                {
                                    put_possible = true;
                                    black_samecolor_limit--;
                                }
                            }
                            else
                            {
                                if (red_samecolor_limit > 0)
                                {
                                    put_possible = true;
                                    red_samecolor_limit--;
                                }
                            }
                        }

                        if (pile.Count > 0 && SelPile.Count == 1
                            && (int)SelPile[0].Range - (int)pile[pile.Count - 1].Range == 1
                            && !SelPile[0].IsOtherColorCard(pile[pile.Count - 1]))

                            put_possible = true;

                        if (put_possible)
                        {
                            DestPile = pile;
                            break;
                        }
                    }

                    if (!put_possible) foreach (CardPile pile in soursepiles)
                        {
                            if (pile.Count == 0 && SelPile[0].Range == Ranges.King && pile != OwnerPile)
                                put_possible = true;

                            if (pile.Count > 0 && pile != OwnerPile
                                && (int)SelPile[0].Range - (int)pile[pile.Count - 1].Range == -1
                                && SelPile[0].IsOtherColorCard(pile[pile.Count - 1]))

                                put_possible = true;

                            if (put_possible)
                            {
                                DestPile = pile;
                                break;
                            }
                        }

                    if (DestPile == null)
                    {
                        OwnerPile.PutPile(SelPile);
                        return;
                    }

                    if (DestPile.Count == 0)
                    {
                        dx = (DestPile.X - SelPile.X) / number_steps;
                        dy = (DestPile.Y - SelPile.Y) / number_steps;
                    }
                    else
                    {
                        dx = (DestPile.Last().X - SelPile.X) / number_steps;
                        dy = (DestPile.Last().Y - SelPile.Y) / number_steps;
                    }

                    float x0 = SelPile.X;
                    float y0 = SelPile.Y;
                    g = Graphics.FromHwnd(ClientArea.Handle);

                    for (int j = 1; j < number_steps; j++)
                    {
                        ClearPileImage(SelPile, ClientArea, g);

                        SelPile.X = x0 + dx * j;
                        SelPile.Y = y0 + dy * j;

                        SelPile.Draw(ClientArea, 1, g);
                        System.Threading.Thread.Sleep(10);
                        //MessageBox.Show("!");
                    }

                    //MessageBox.Show("!");
                    ClearPileImage(SelPile, ClientArea, Graphics.FromHwnd(ClientArea.Handle));
                    DestPile.PutPile(SelPile);
                    //MessageBox.Show("!");
                    SelPile.Draw(ClientArea, 1, Graphics.FromHwnd(ClientArea.Handle));

                    AdaptScrolling();
                    
                    FixedUzerAction(OwnerPile, DestPile, SelPile);

                    if (first_dest_piles.Contains(DestPile))
                        Counter += (int)SelPile[0].Range;
                    CounterLabel.Text = "Cчёт: " + Counter.ToString();

                    lastcard = OwnerPile.LastOrDefault();
                    if (lastcard != null && lastcard.CardBack == true)
                    {
                        OwnerPile.Remove(lastcard);
                        TurnOverTheCard(lastcard);
                        OwnerPile.Add(lastcard);
                    }

                    if (SuiteComplectCreate(DestPile))
                    {                        
                        StartPile = DestPile;
                        DestPile = second_dest_piles.First(pile => pile.Count == 0);
                        MoveSuiteComplect(StartPile, DestPile);
                        FixedUzerAction(StartPile, DestPile, null);

                        lastcard = StartPile.LastOrDefault();
                        if (lastcard != null && lastcard.CardBack == true)
                        {
                            StartPile.Remove(lastcard);
                            TurnOverTheCard(lastcard);
                            StartPile.Add(lastcard);
                        }


                        foreach (Card c in DestPile)
                        {
                            Counter += (int)((int)c.Range * 1.2);
                            CounterLabel.Text = "Cчёт: " + Counter.ToString();
                        }
                    }

                    SelPile = null;
                }

                foreach (CardPile pile in first_dest_piles)
                    if (pile.Count < 13) return;

                if (black_samecolor_limit + black_altcolor_limit + red_altcolor_limit + red_samecolor_limit == 0)
                {
                    DialogResult res = new ResultsForm().ShowDialog(this);

                    if (res == DialogResult.Yes)
                    {
                        ClientArea.Refresh();
                        NewGameIsBegin = false;
                        NewGame_Click(this, null);
                    }
                    else Close();
                }
            }
        }

        private void TurnOverTheCard(Card card)
        {
            ClearCardImage(card, ClientArea, Graphics.FromHwnd(ClientArea.Handle));
            card.X -= 10;
            card.Y -= 10;
            card.Draw(ClientArea, Graphics.FromHwnd(ClientArea.Handle));
            ClearCardImage(card, ClientArea, Graphics.FromHwnd(ClientArea.Handle));
            card.CardBack = false;
            card.Draw(ClientArea, Graphics.FromHwnd(ClientArea.Handle));
            System.Threading.Thread.Sleep(100);

            ClearCardImage(card, ClientArea, Graphics.FromHwnd(ClientArea.Handle));
            card.X += 10;
            card.Y += 10;
            card.Draw(ClientArea, Graphics.FromHwnd(ClientArea.Handle));
        }

        private void ClientArea_MouseDown(object sender, MouseEventArgs e)
        {
            Graphics g, BufGraph;
            Card card;
            CardPile OwnPile = null;
            RectangleF drawrect;

            if (NewGameIsBegin && !dragging)
            {
                card = GetItemAt(e.X, e.Y);

                if (card == null || e.Button == MouseButtons.Middle || e.Clicks > 1) return;

                if (soursepiles.Contains(card.OwnerPile))
                {
                    OwnPile = card.OwnerPile;

                    for (int i = OwnPile.IndexOf(card); i < OwnPile.Count; i++)
                        if (OwnPile[i].CardBack) return;

                    for (int i = OwnPile.Count - 2; i >= OwnPile.IndexOf(card); i--)
                    {
                        bool isothercolor = OwnPile[i].IsOtherColorCard(OwnPile[i + 1]);
                        if (((int)OwnPile[i].Range) - ((int)OwnPile[i + 1].Range) != 1
                            || !isothercolor)
                            return;
                    }

                    SelPile = OwnPile.SelectPile(OwnPile.IndexOf(card));
                    startX = SelPile.X;
                    startY = SelPile.Y;
                    StartPile = OwnPile;

                    g = Graphics.FromHwnd(ClientArea.Handle);
                    BufGraph = Graphics.FromImage(DoubleBuffer);

                    SelPile.X -= 10;
                    SelPile.Y -= 10;
                    StartDragPoint = e.Location;

                    drawrect = new RectangleF(new PointF(SelPile.X, SelPile.Y),
                                              new SizeF(SelPile.PileWidth + 10,
                                                        SelPile.PileHeight + 10));

                    BufGraph.FillRectangle(new SolidBrush(ClientArea.BackColor), drawrect);
                    DrawPiles(ClientArea, drawrect, BufGraph, null);
                    SelPile.Draw(ClientArea, 1, BufGraph);

                    g.DrawImage(DoubleBuffer, SelPile.X, SelPile.Y,
                                drawrect, GraphicsUnit.Pixel);
                    
                    dragging = true;
                    ClientArea.Cursor = Cursors.Hand;
                    System.Windows.Forms.Cursor.Clip = ClientArea.RectangleToScreen(ClientArea.ClientRectangle);

                    g.Dispose();
                    BufGraph.Dispose();
                    drawrect = Rectangle.Empty;
                    GC.Collect();
                }
            }
        }

        private void ClientArea_MouseMove(object sender, MouseEventArgs e)
        {            
            Graphics BufGraph, g;
            int dx, dy;
            float drawX, drawY;
            RectangleF drawrect;
            CardPile SelectPile;
            Cell cell = null;
                        
            if (NewGameIsBegin)
            {
                foreach (Cell c in soursecells)
                {
                    RectangleF r = c.DockPile.LocationRect;// new RectangleF(c.DockPile.LocationRect..Location, c.Size);
                    if (r.Contains(e.X, e.Y))
                    {
                        cell = c;
                        break;
                    }
                }

                if (cell == null) foreach (Cell c in first_dest_cells)
                    {
                        RectangleF r = c.DockPile.LocationRect; // new RectangleF(c.Location, c.Size);
                        if (r.Contains(e.X, e.Y))
                        {
                            cell = c;
                            break;
                        }
                    }

                if (cell == null) foreach (Cell c in second_dest_cells)
                    {
                        RectangleF r = c.DockPile.LocationRect;// new RectangleF(c.Location, c.Size);
                        if (r.Contains(e.X, e.Y))
                        {
                            cell = c;
                            break;
                        }
                    }
                
                if (cell == null)
                {
                    RectangleF r = cardbatch.LocationRect;
                    //RectangleF r =  new RectangleF(cardbatch.DockCell.Location,
                    //                            cardbatch.DockCell.Size);

                    if (r.Contains(e.X, e.Y)) cell = cardbatch.DockCell;
                }

                if (cell == null) SelectPile = null;
                else SelectPile = cell.DockPile;

                if (SelectPile == cardbatch)
                {
                    CurSelecObject.Text = "Колода карт";
                    NumberCards.Text = NameCards(SelectPile.Count);
                    if (SelectPile.Count == 0)
                        InfoLabel.Text = "";
                    else
                        InfoLabel.Text = "Нажмите чтобы разложить новый ряд карт";
                }
                else if (first_dest_piles.Contains(SelectPile))
                {
                    CurSelecObject.Text = "Стопка карт";
                    NumberCards.Text = NameCards(SelectPile.Count);
                    InfoLabel.Text = "Сложите в стопку набор карт одного цвета от туза до короля, чтобы выиграть";
                }
                else if (second_dest_piles.Contains(SelectPile))
                {
                    CurSelecObject.Text = "Стопка карт";
                    NumberCards.Text = NameCards(SelectPile.Count);
                    InfoLabel.Text = "Когда \"Королевский набор будет собран, поместите его в эту стопку, чтобы выиграть";
                }
                else if (soursepiles.Contains(SelectPile))
                {
                    CurSelecObject.Text = "Стопка карт";
                    NumberCards.Text = NameCards(SelectPile.Count);
                    if (SelectPile.Count == 0)
                        InfoLabel.Text = "";
                    else
                        InfoLabel.Text = "Переложите карты из  этой стопки в стопки, расположенные выше, или в другую стопку.";
                }
                else
                {
                    CurSelecObject.Text = "";
                    NumberCards.Text = "";
                    InfoLabel.Text = "";
                }

                if (dragging)
                {
                    g = Graphics.FromHwnd(ClientArea.Handle);
                    BufGraph = Graphics.FromImage(DoubleBuffer);
                    dx = e.X - StartDragPoint.X;
                    dy = e.Y - StartDragPoint.Y;

                    if (e.X - StartDragPoint.X > 0)
                        drawX = SelPile.X;
                    else
                        drawX = SelPile.X + dx;

                    if (e.Y - StartDragPoint.Y > 0)
                        drawY = SelPile.Y;
                    else
                        drawY = SelPile.Y + dy;

                    drawrect = new RectangleF(new PointF(drawX, drawY),
                                              new SizeF(SelPile.PileWidth + Math.Abs(dx),
                                                        SelPile.PileHeight + Math.Abs(dy)));

                    SelPile.X = SelPile.X + dx;
                    SelPile.Y = SelPile.Y + dy;
                    StartDragPoint = e.Location;

                    BufGraph.FillRectangle(new SolidBrush(ClientArea.BackColor), drawrect);
                    DrawPiles(ClientArea, drawrect, BufGraph, null);
                    SelPile.Draw(ClientArea, 1, BufGraph);

                    g.DrawImage(DoubleBuffer, drawX, drawY,
                                drawrect, GraphicsUnit.Pixel);
                }
            }
        }

        private void ClientArea_MouseUp(object sender, MouseEventArgs e)
        {
            Graphics g, BufGraph;
            Card lastcard;
            CardPile DestPile = null;            
            bool put_possible = false;
            RectangleF drawrect;

            if (NewGameIsBegin)
            {
                if (dragging)
                {                    
                    dragging = false;
                    ClientArea.Cursor = Cursors.Default;
                    System.Windows.Forms.Cursor.Clip = Rectangle.Empty;

                    DestPile = GetItemAt(SelPile);

                    if (first_dest_piles.Contains(DestPile) && DestPile.Count == 0
                        && SelPile.Count == 1 && SelPile[0].Range == Ranges.Ace)
                    {
                        if (SelPile[0].Suite == Suits.Clubs || SelPile[0].Suite == Suits.Spades)
                        {
                            if (black_samecolor_limit > 0)
                            {
                                put_possible = true;
                                black_samecolor_limit--;
                            }
                        }
                        else
                        {
                            if (red_samecolor_limit > 0)
                            {
                                put_possible = true;
                                red_samecolor_limit--;
                            }
                        }
                    }

                    if (soursepiles.Contains(DestPile) && DestPile.Count == 0
                        && SelPile[0].Range == Ranges.King && DestPile != StartPile)

                        put_possible = true;

                    if (first_dest_piles.Contains(DestPile)
                        && DestPile.Count > 0 && SelPile.Count == 1
                        && (int)SelPile[0].Range - (int)DestPile[DestPile.Count - 1].Range == 1
                        && !SelPile[0].IsOtherColorCard(DestPile[DestPile.Count - 1]))

                        put_possible = true;

                    if (soursepiles.Contains(DestPile)
                        && DestPile.Count > 0 && DestPile != StartPile
                        && (int)SelPile[0].Range - (int)DestPile[DestPile.Count - 1].Range == -1
                        && SelPile[0].IsOtherColorCard(DestPile[DestPile.Count - 1]))

                        put_possible = true;


                    if (put_possible)
                    {
                        float dx, dy, oldX, oldY, drawX, drawY;

                        g = Graphics.FromHwnd(ClientArea.Handle);
                        BufGraph = Graphics.FromImage(DoubleBuffer);

                        oldX = SelPile.X;
                        oldY = SelPile.Y;

                        DestPile.PutPile(SelPile);

                        dx = SelPile.X - oldX;
                        dy = SelPile.Y - oldY;

                        if (dx > 0) drawX = oldX;
                        else drawX = SelPile.X;

                        if (dy > 0) drawY = oldY;
                        else drawY = SelPile.Y;
                        
                        drawrect = new RectangleF(new PointF(drawX, drawY),
                                                  new SizeF(SelPile.PileWidth + Math.Abs(dx),
                                                            SelPile.PileHeight + Math.Abs(dy)));

                        BufGraph.FillRectangle(new SolidBrush(ClientArea.BackColor), drawrect);
                        DrawPiles(ClientArea, drawrect, BufGraph, null);
                        SelPile.Draw(ClientArea, 1, BufGraph);

                        g.DrawImage(DoubleBuffer, drawX, drawY,
                                    drawrect, GraphicsUnit.Pixel);

                        FixedUzerAction(StartPile, DestPile, SelPile);
                       
                        if (first_dest_piles.Contains(DestPile))
                            Counter += (int)SelPile[0].Range;

                        CounterLabel.Text = "Счёт: " + Counter.ToString();
                    }
                    else
                    {
                        float dx, dy, x0, y0, drawX, drawY;

                        dx = (startX - SelPile.X) / ((int)(1.5 * number_steps));                        
                        dy = (startY - SelPile.Y) / ((int)(1.5 * number_steps));

                        x0 = SelPile.X;
                        y0 = SelPile.Y;
                        g = Graphics.FromHwnd(ClientArea.Handle);
                        BufGraph = Graphics.FromImage(DoubleBuffer);

                        for (int j = 1; j <= (int)(1.5 * number_steps); j++)
                        {
                            if (dx > 0) drawX = SelPile.X;
                            else drawX = SelPile.X + dx;

                            if (dy > 0) drawY = SelPile.Y;
                            else drawY = SelPile.Y + dy;

                            drawrect = new RectangleF(new PointF(drawX, drawY),
                                                      new SizeF(SelPile.PileWidth + Math.Abs(dx),
                                                                SelPile.PileHeight + Math.Abs(dy)));

                            SelPile.X = x0 + dx * j;
                            SelPile.Y = y0 + dy * j;
                            StartDragPoint = e.Location;

                            BufGraph.FillRectangle(new SolidBrush(ClientArea.BackColor), drawrect);
                            DrawPiles(ClientArea, drawrect, BufGraph, null);
                            SelPile.Draw(ClientArea, 1, BufGraph);

                            g.DrawImage(DoubleBuffer, drawX, drawY,
                                        drawrect, GraphicsUnit.Pixel);
                            System.Threading.Thread.Sleep(2);                            
                        }

                        SelPile.X = startX;
                        SelPile.Y = startY;
                        StartPile.PutPile(SelPile);
                    }

                    if (put_possible)
                    {
                        lastcard = StartPile.LastOrDefault();
                        if (lastcard != null && lastcard.CardBack == true)
                        {
                            StartPile.Remove(lastcard);
                            TurnOverTheCard(lastcard);
                            StartPile.Add(lastcard);
                        }

                        if (SuiteComplectCreate(DestPile))
                        {
                            StartPile = DestPile;
                            DestPile = second_dest_piles.First(pile => pile.Count == 0);
                            MoveSuiteComplect(StartPile, DestPile);
                            FixedUzerAction(StartPile, DestPile, null);

                            lastcard = StartPile.LastOrDefault();
                            if (lastcard != null && lastcard.CardBack == true)
                            {
                                StartPile.Remove(lastcard);
                                TurnOverTheCard(lastcard);
                                StartPile.Add(lastcard);
                            }
                            
                            foreach (Card card in DestPile)
                            {
                                Counter += (int)((int)card.Range * 1.2);
                                CounterLabel.Text = "Cчёт: " + Counter.ToString();
                            }
                        }

                        AdaptScrolling();
                    }
                    SelPile = null;
                }

                foreach (CardPile pile in first_dest_piles)
                    if (pile.Count < 13) return;

                if (black_samecolor_limit + black_altcolor_limit + red_altcolor_limit + red_samecolor_limit == 0)
                {
                    DialogResult res = new ResultsForm().ShowDialog(this);

                    if (res == DialogResult.Yes)
                    {
                        ClientArea.Refresh();
                        NewGameIsBegin = false;
                        NewGame_Click(this, null);
                    }
                    else Close();
                }
            }
        }

        private void AdaptScrolling()
        {
            float MaxHeight = 0;


            MaxHeight = soursepiles.Max(pile => pile.Y + pile.PileHeight);

            if (MaxHeight + ScrollBar.Value > ClientArea.Height)
            {
                ScrollBar.Maximum = (int)MaxHeight - ClientArea.Height + 20 + ScrollBar.LargeChange + ScrollBar.Value;
                if (!ScrollBar.Visible) ScrollBar.Visible = true;
            }
            else
            {
                ScrollBar.Visible = false;
                if (ScrollBar.Value != 0)
                {                    
                    cardbatchcell.Y += ScrollBar.Value;

                    foreach (Cell cell in first_dest_cells)
                        cell.Y += ScrollBar.Value;

                    foreach (Cell cell in second_dest_cells)
                        cell.Y += ScrollBar.Value;

                    foreach (Cell cell in soursecells)
                        cell.Y += ScrollBar.Value;
                    
                    ScrollBar.Value = 0;
                    ClientArea.Invalidate();
                }                                 
            }            
        }

        private bool SuiteComplectCreate(CardPile SuspectPile)
        {
            if (!first_dest_piles.Contains(SuspectPile)
                && SuspectPile.Count >= 13 && SuspectPile.Last().Range == Ranges.Ace
                && SuspectPile[SuspectPile.Count - 13].Range == Ranges.King
                && SuspectPile[SuspectPile.Count - 13].CardBack == false
                && !SuspectPile.Last().IsOtherColorCard(SuspectPile[SuspectPile.Count - (int)Ranges.King]))
            {
                for (int i = (int)Ranges.Ace; i < (int)Ranges.King; i++)
                {
                    Card FirstCard = SuspectPile[SuspectPile.Count - i - 1];
                    Card SecondCard = SuspectPile[SuspectPile.Count - i];

                    if ((int)FirstCard.Range - (int)SecondCard.Range != 1
                        || FirstCard.CardBack == true || SecondCard.CardBack == true
                        || !FirstCard.IsOtherColorCard(SecondCard))
                        
                        return false;
                }

                if (SuspectPile[SuspectPile.Count - (int)Ranges.King].Suite == Suits.Diamonds
                    || SuspectPile[SuspectPile.Count - (int)Ranges.King].Suite == Suits.Hearts)
                {
                    if (red_altcolor_limit > 0)
                    {
                        red_altcolor_limit--;
                        return true;
                    }
                    else return false;
                }
                else
                {
                    if (black_altcolor_limit > 0)
                    {
                        black_altcolor_limit--;
                        return true;
                    }
                    else return false;
                }
            }
            return false;
        }

        private void MoveSuiteComplect(CardPile StartPile, CardPile DestPile)
        {
            Graphics g = Graphics.FromHwnd(ClientArea.Handle);

            for (int i = (int)Ranges.Ace; i <= (int)Ranges.King; i++)
            {
                Card movecard = StartPile.Last();
                float dx, dy,x0, y0;
                
                StartPile.Remove(movecard);
                if (DestPile.LastOrDefault() == null)
                {
                    dx = (DestPile.X - movecard.X) / number_steps;
                    dy = (DestPile.Y - movecard.Y) / number_steps;
                }
                else
                {
                    dx = (DestPile.Last().X - movecard.X) / number_steps;
                    dy = (DestPile.Last().Y - movecard.Y) / number_steps;
                }
                x0 = movecard.X;
                y0 = movecard.Y;

                for (int j = 1; j < number_steps; j++)
                {
                    ClearCardImage(movecard, ClientArea, g);
                    movecard.X = x0 + dx * j;
                    movecard.Y = y0 + dy * j;
                    movecard.Draw(ClientArea, g);
                    System.Threading.Thread.Sleep(5);
                }
                ClearCardImage(movecard, ClientArea, g);
                DestPile.Add(movecard);
                movecard.Draw(ClientArea, g);
                System.Threading.Thread.Sleep(2);
            }
        }

        private void FixedUzerAction(CardPile StartPile, CardPile DestPile, CardPile SelPile)
        {
            FixedUzerAction(StartPile, 
                            new CardPile[] { DestPile }, 
                            new CardPile[] { SelPile });
        }

        private void FixedUzerAction(CardPile StartPile, CardPile[] DestPile, CardPile[] SelPile)
        {
            CurUserAction++;
            if (CurUserAction < UserActionSequence.Count)
            {
                UserActionSequence.RemoveRange(CurUserAction,
                                               UserActionSequence.Count - CurUserAction);
                MenuItem_Redo.Enabled = false;
                ToolButton_Redo.Enabled = false;
            }
            UserActionSequence.Add(new CardPileInfo(StartPile, DestPile, SelPile));

            MenuItem_Undo.Enabled = true;
            ToolButton_Undo.Enabled = true;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            MainMenu.Show();
            MainToolStrip.Show();
            Context_Menu.Show();

            MinimumSize = new Size(Size.Width, Size.Height);
            DoubleBuffer = new Bitmap(ClientArea.Width, ClientArea.Height);
        }
        
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics bufg;
            if (NewGameIsBegin)
            {
                bufg = Graphics.FromImage(DoubleBuffer);

                bufg.FillRectangle(new SolidBrush(ClientArea.BackColor),
                                   e.ClipRectangle);
                DrawPiles(ClientArea, e.ClipRectangle, bufg, null);
                e.Graphics.DrawImage(DoubleBuffer, 
                                     e.ClipRectangle, e.ClipRectangle, 
                                     GraphicsUnit.Pixel);
                bufg.Dispose();
            }
        }

        private void NewGame_Click(object sender, EventArgs e)
        {
            DialogResult res;
            Graphics g;
            List<Card> cardset;
            if (NewGameIsBegin)
            {
                res = MessageBox.Show("Результаты текущей игры будут потеряны. Продолжить?",
                                     "Cообщение", MessageBoxButtons.YesNo,
                                     MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (res == DialogResult.No) return;
            }

            GameTimer.Stop();

            CounterLabel.Visible = true;
            GameTimeLabel.Visible = true;

            Counter = 0;
            CounterLabel.Text = "Счёт: 0";

            seconds = 0;
            GameTimeLabel.Text = "Время: 00:00:00";

            ScrollBar.Value = 0;
            ScrollBar.Visible = false;

            #region Код, формирующий в памяти новый игровой набор

            cardset = GenerateCardSet(2);

            width = (Width - 12 * depthx) / 10;
            height = 150f * (width / 98.0f);

            cardbatchcell = new Cell(CellType.SetSuiteCell, depthx, depthy + 20, width, height);
            cardbatch = new CardPile(cardset, depthx, depthy + 20, -0.1f, -0.1f);
            cardbatch.DockCell = cardbatchcell;
            cardbatch.DockCell.DockPile = cardbatch;
            for (int i = 0; i < soursecells.Length; i++)
            {
                float curx = (depthx + width) * i + depthx;

                soursecells[i] = new Cell(CellType.KingCell, curx,
                                          cardbatch.Y + cardbatch.CardHeight + depthy,
                                          width, height);

                soursepiles[i] = new CardPile(soursecells[i].X, soursecells[i].Y, 0f, 5f);

                soursepiles[i].DockCell = soursecells[i];
                soursecells[i].DockPile = soursepiles[i];
            }

            for (int i = 0; i < first_dest_cells.Length; i++)
            {
                float curx = (depthx + width) * (i + 2) + depthx;

                first_dest_cells[i] = new Cell(CellType.AceCell, curx, depthy + 20,
                                               width, height);

                first_dest_piles[i] = new CardPile(first_dest_cells[i].X,
                                                   first_dest_cells[i].Y, -0.5f, -0.5f);

                first_dest_piles[i].DockCell = first_dest_cells[i];
                first_dest_cells[i].DockPile = first_dest_piles[i];
            }

            for (int i = 0; i < second_dest_cells.Length; i++)
            {
                float curx = (depthx + width) * (i + 6) + depthx;

                second_dest_cells[i] = new Cell(CellType.EmptyCell, curx, depthy + 20,
                                               width, height);

                second_dest_piles[i] = new CardPile(second_dest_cells[i].X,
                                                   second_dest_cells[i].Y, -0.5f, -0.5f);

                second_dest_piles[i].DockCell = second_dest_cells[i];
                second_dest_cells[i].DockPile = second_dest_piles[i];
            }
            GC.Collect();
            #endregion

            #region Код, отрисовывающий новый игровой набор
            Refresh();
            g = Graphics.FromHwnd(ClientArea.Handle);
            g.FillRectangle(new SolidBrush(ClientArea.BackColor), ClientArea.ClientRectangle);
            cardbatch.Draw(ClientArea, 20, g);
            foreach (CardPile pile in soursepiles) pile.Draw(ClientArea, 1, g);
            foreach (CardPile pile in first_dest_piles) pile.Draw(ClientArea, 1, g);
            foreach (CardPile pile in second_dest_piles) pile.Draw(ClientArea, 1, g);

            for (int i = soursepiles.Length - 1; i > 0; i--)
                SpreadTheCards(cardbatch, soursepiles.Take(i).ToArray(), false);
            #endregion

            NewGameIsBegin = true;
            black_samecolor_limit = 2;
            red_samecolor_limit = 2;
            black_altcolor_limit = 2;
            red_altcolor_limit = 2;
            UserActionSequence = new List<CardPileInfo>();
            CurUserAction = -1;
            MenuItem_Undo.Enabled = false;
            MenuItem_Redo.Enabled = false;
            ToolButton_Undo.Enabled = false;
            ToolButton_Redo.Enabled = false;
            GameTimer.Start();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Undo_Click(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromHwnd(ClientArea.Handle);
            CardPile SourcePile, DestPile, MovePile;
            CardPile[] DestPiles, MovePiles;
            bool LastCardBack;

            SourcePile = UserActionSequence[CurUserAction].SourcePile;
            DestPiles = UserActionSequence[CurUserAction].DestPiles;            
            MovePiles = UserActionSequence[CurUserAction].MovePiles;            
            LastCardBack = UserActionSequence[CurUserAction].LastCardBack;

            if (SourcePile == cardbatch)
            {
                for (int i = MovePiles.Length - 1; i >= 0; i--)
                {
                    MovePile = MovePiles[i];
                    DestPile = DestPiles[i];

                    DestPile.SelectPile(DestPile.IndexOf(MovePile[0]));
                    ClearPileImage(MovePile, ClientArea, g);                                        
                    SourcePile.PutPile(MovePile);
                    SourcePile.Last().CardBack = true;
                }
                SourcePile.Draw(ClientArea, 20, g);
            }
            else
            {
                DestPile = DestPiles[0];
                MovePile = MovePiles[0];

                if (second_dest_piles.Contains(DestPile))
                {
                    for (int i = (int)Ranges.King; i >= (int)Ranges.Ace; i--)
                    {
                        Card movecard = DestPile.Last();

                        Counter -= (int)((int)movecard.Range * 1.2);
                        DestPile.Remove(movecard);
                        ClearCardImage(movecard, ClientArea, g);
                        SourcePile.Add(movecard);
                        movecard.Draw(ClientArea, g);
                    }

                    CounterLabel.Text = "Счет: " + Counter.ToString();
                    if (SourcePile.Last().Suite == Suits.Diamonds
                        || SourcePile.Last().Suite == Suits.Hearts)
                        red_altcolor_limit++;                        
                    else
                        black_altcolor_limit++;

                    CurUserAction--;
                    SourcePile = UserActionSequence[CurUserAction].SourcePile;
                    DestPile = UserActionSequence[CurUserAction].DestPiles[0];
                    MovePile = UserActionSequence[CurUserAction].MovePiles[0];                    
                }

                DestPile.SelectPile(DestPile.IndexOf(MovePile[0]));
                ClearPileImage(MovePile, ClientArea, g);
                if (LastCardBack && SourcePile.Count > 0)
                    SourcePile.LastOrDefault().CardBack = LastCardBack;
                SourcePile.PutPile(MovePile);
                MovePile.Draw(ClientArea, 1, g);

                if (first_dest_piles.Contains(DestPile))
                {
                    Counter -= (int)MovePile[0].Range;
                    CounterLabel.Text = "Счет: " + Counter.ToString();
                    if (MovePile[0].Range == Ranges.Ace)
                    {
                        if (MovePile[0].Suite == Suits.Diamonds
                            || MovePile[0].Suite == Suits.Hearts)
                            red_samecolor_limit++;
                        else
                            black_samecolor_limit++;
                    }
                }

            }
      
            CurUserAction--;
            MenuItem_Redo.Enabled = true;
            ToolButton_Redo.Enabled = true;
            if (CurUserAction < 0)
            {
                MenuItem_Undo.Enabled = false;
                ToolButton_Undo.Enabled = false;
            }
            AdaptScrolling();
        }

        private void Redo_Click(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromHwnd(ClientArea.Handle);
            CardPile SourcePile, DestPile, MovePile;
            CardPile[] DestPiles, MovePiles;
            
            CurUserAction++;
            SourcePile = UserActionSequence[CurUserAction].SourcePile;            
            DestPiles = UserActionSequence[CurUserAction].DestPiles;            
            MovePiles = UserActionSequence[CurUserAction].MovePiles;

            if (SourcePile == cardbatch)
            {
                for (int i = 0; i < MovePiles.Length; i++)
                {
                    MovePile = MovePiles[i];
                    DestPile = DestPiles[i];

                    SourcePile.SelectPile(SourcePile.IndexOf(MovePile[0]));                    
                    ClearPileImage(MovePile, ClientArea, g);
                    DestPile.PutPile(MovePile);
                    DestPile.Last().CardBack = false;
                    MovePile.Draw(ClientArea, 1, g);
                }
            }
            else
            {
                bool FormingKingSuit;
                MovePile = MovePiles[0];
                DestPile = DestPiles[0];

                if (UserActionSequence.Count == CurUserAction + 1)
                    FormingKingSuit = false;
                else FormingKingSuit = second_dest_piles.
                    Contains(UserActionSequence[CurUserAction + 1].DestPiles[0]);                

                SourcePile.SelectPile(SourcePile.IndexOf(MovePile[0]));
                if (SourcePile.Count > 0) SourcePile.Last().CardBack = false;
                ClearPileImage(MovePile, ClientArea, g);
                DestPile.PutPile(MovePile);
                if (!FormingKingSuit) MovePile.Draw(ClientArea, 1, g);

                if (first_dest_piles.Contains(DestPile))
                {
                    Counter += (int)MovePile[0].Range;
                    CounterLabel.Text = "Счет: " + Counter.ToString();
                    if (MovePile[0].Range == Ranges.Ace)
                    {
                        if (MovePile[0].Suite == Suits.Diamonds 
                            || MovePile[0].Suite == Suits.Hearts)
                            red_samecolor_limit--;
                        else
                            black_samecolor_limit--;
                    }
                }

                if (FormingKingSuit)
                {
                    CurUserAction++;
                    SourcePile = UserActionSequence[CurUserAction].SourcePile;
                    DestPile = UserActionSequence[CurUserAction].DestPiles[0];
                    MovePile = UserActionSequence[CurUserAction].MovePiles[0];

                    for (int i = (int)Ranges.Ace; i <= (int)Ranges.King; i++)
                    {
                        Card movecard = SourcePile.Last();

                        Counter += (int)((int)movecard.Range * 1.2);
                        SourcePile.Remove(movecard);
                        ClearCardImage(movecard, ClientArea, g);
                        DestPile.Add(movecard);
                        movecard.Draw(ClientArea, g);
                    }

                    CounterLabel.Text = "Счет: " + Counter.ToString();
                    if (DestPile.Last().Suite == Suits.Diamonds
                        || DestPile.Last().Suite == Suits.Hearts)
                        red_altcolor_limit--;
                    else
                        black_altcolor_limit--;
                }
            }
                        
            MenuItem_Undo.Enabled = true;
            ToolButton_Undo.Enabled = true;
            if (CurUserAction == UserActionSequence.Count - 1)
            {
                MenuItem_Redo.Enabled = false;
                ToolButton_Redo.Enabled = false;
            }
            AdaptScrolling();
        }

        private void AboutProgram_Click(object sender, EventArgs e)
        {
            AboutForm form = new AboutForm();
            form.ShowDialog(this);
        }

        private void Rules_Click(object sender, EventArgs e)
        {
            RulesForm form = new RulesForm();
            form.Show(this);
        }

        private void ScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.NewValue - e.OldValue == 0) return;
            cardbatchcell.Y = cardbatchcell.Y - (e.NewValue - e.OldValue);
            
            foreach (Cell cell in first_dest_cells)
                cell.Y = cell.Y - (e.NewValue - e.OldValue);

            foreach (Cell cell in second_dest_cells)
                cell.Y = cell.Y - (e.NewValue - e.OldValue);

            foreach (Cell cell in soursecells)
                cell.Y = cell.Y - (e.NewValue - e.OldValue);
            ClientArea.Invalidate();
        }

        private void GameTimer_Tick(object sender, EventArgs e)
        {
            int ghours, gminutes, gseconds;

            seconds++;

            ghours = seconds / 3600;
            gminutes = (seconds % 3600) / 60;
            gseconds = seconds % 60;

            GameTimeLabel.Text = "Время: " + ghours.ToString("D2") + ":" 
                                 + gminutes.ToString("D2") + ":" 
                                 + gseconds.ToString("D2");
        }
    }
}
