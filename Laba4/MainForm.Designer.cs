﻿namespace RogueAtNoir
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.MenuItem_File = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_NewGame = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_Game = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_Undo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_Redo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_Rules = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_AboutProgram = new System.Windows.Forms.ToolStripMenuItem();
            this.MainToolStrip = new System.Windows.Forms.ToolStrip();
            this.ToolButton_NewGame = new System.Windows.Forms.ToolStripButton();
            this.ToolButton_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolButton_Undo = new System.Windows.Forms.ToolStripButton();
            this.ToolButton_Redo = new System.Windows.Forms.ToolStripButton();
            this.ToolButton_Rules = new System.Windows.Forms.ToolStripButton();
            this.GameTimeLabel = new System.Windows.Forms.ToolStripLabel();
            this.CounterLabel = new System.Windows.Forms.ToolStripLabel();
            this.ClientArea = new System.Windows.Forms.Panel();
            this.Context_Menu = new System.Windows.Forms.StatusStrip();
            this.CurSelecObject = new System.Windows.Forms.ToolStripStatusLabel();
            this.Splitter1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.NumberCards = new System.Windows.Forms.ToolStripStatusLabel();
            this.Splitter2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.InfoLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ScrollBar = new System.Windows.Forms.VScrollBar();
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MainMenu.SuspendLayout();
            this.MainToolStrip.SuspendLayout();
            this.Context_Menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_File,
            this.MenuItem_Game,
            this.MenuItem_Help});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(724, 24);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "menuStrip1";
            // 
            // MenuItem_File
            // 
            this.MenuItem_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_NewGame,
            this.MenuItem_Exit});
            this.MenuItem_File.Name = "MenuItem_File";
            this.MenuItem_File.Size = new System.Drawing.Size(45, 20);
            this.MenuItem_File.Text = "&Файл";
            // 
            // MenuItem_NewGame
            // 
            this.MenuItem_NewGame.Name = "MenuItem_NewGame";
            this.MenuItem_NewGame.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.MenuItem_NewGame.Size = new System.Drawing.Size(161, 22);
            this.MenuItem_NewGame.Text = "Новая игра";
            this.MenuItem_NewGame.Click += new System.EventHandler(this.NewGame_Click);
            // 
            // MenuItem_Exit
            // 
            this.MenuItem_Exit.Name = "MenuItem_Exit";
            this.MenuItem_Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.MenuItem_Exit.Size = new System.Drawing.Size(161, 22);
            this.MenuItem_Exit.Text = "Выход";
            this.MenuItem_Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // MenuItem_Game
            // 
            this.MenuItem_Game.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_Undo,
            this.MenuItem_Redo});
            this.MenuItem_Game.Name = "MenuItem_Game";
            this.MenuItem_Game.Size = new System.Drawing.Size(43, 20);
            this.MenuItem_Game.Text = "&Игра";
            // 
            // MenuItem_Undo
            // 
            this.MenuItem_Undo.Enabled = false;
            this.MenuItem_Undo.Name = "MenuItem_Undo";
            this.MenuItem_Undo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.MenuItem_Undo.Size = new System.Drawing.Size(193, 22);
            this.MenuItem_Undo.Text = "Отменить";
            this.MenuItem_Undo.Click += new System.EventHandler(this.Undo_Click);
            // 
            // MenuItem_Redo
            // 
            this.MenuItem_Redo.Enabled = false;
            this.MenuItem_Redo.Name = "MenuItem_Redo";
            this.MenuItem_Redo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.MenuItem_Redo.Size = new System.Drawing.Size(193, 22);
            this.MenuItem_Redo.Text = "Восстановить";
            this.MenuItem_Redo.Click += new System.EventHandler(this.Redo_Click);
            // 
            // MenuItem_Help
            // 
            this.MenuItem_Help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_Rules,
            this.MenuItem_AboutProgram});
            this.MenuItem_Help.Name = "MenuItem_Help";
            this.MenuItem_Help.Size = new System.Drawing.Size(59, 20);
            this.MenuItem_Help.Text = "&Помощь";
            // 
            // MenuItem_Rules
            // 
            this.MenuItem_Rules.Name = "MenuItem_Rules";
            this.MenuItem_Rules.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.MenuItem_Rules.Size = new System.Drawing.Size(149, 22);
            this.MenuItem_Rules.Text = "Правила";
            this.MenuItem_Rules.Click += new System.EventHandler(this.Rules_Click);
            // 
            // MenuItem_AboutProgram
            // 
            this.MenuItem_AboutProgram.Name = "MenuItem_AboutProgram";
            this.MenuItem_AboutProgram.Size = new System.Drawing.Size(149, 22);
            this.MenuItem_AboutProgram.Text = "О программе";
            this.MenuItem_AboutProgram.Click += new System.EventHandler(this.AboutProgram_Click);
            // 
            // MainToolStrip
            // 
            this.MainToolStrip.AutoSize = false;
            this.MainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolButton_NewGame,
            this.ToolButton_Exit,
            this.toolStripSeparator1,
            this.ToolButton_Undo,
            this.ToolButton_Redo,
            this.toolStripSeparator3,
            this.ToolButton_Rules,
            this.GameTimeLabel,
            this.toolStripSeparator2,
            this.CounterLabel});
            this.MainToolStrip.Location = new System.Drawing.Point(0, 24);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Size = new System.Drawing.Size(724, 41);
            this.MainToolStrip.TabIndex = 1;
            this.MainToolStrip.Text = "toolStrip1";
            // 
            // ToolButton_NewGame
            // 
            this.ToolButton_NewGame.Image = global::RogueAtNoir.Properties.Resources.cards_shlp1;
            this.ToolButton_NewGame.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolButton_NewGame.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolButton_NewGame.Name = "ToolButton_NewGame";
            this.ToolButton_NewGame.Size = new System.Drawing.Size(117, 38);
            this.ToolButton_NewGame.Text = "Новая игра";
            this.ToolButton_NewGame.Click += new System.EventHandler(this.NewGame_Click);
            // 
            // ToolButton_Exit
            // 
            this.ToolButton_Exit.Image = global::RogueAtNoir.Properties.Resources.exit_32x32;
            this.ToolButton_Exit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolButton_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolButton_Exit.Name = "ToolButton_Exit";
            this.ToolButton_Exit.Size = new System.Drawing.Size(76, 38);
            this.ToolButton_Exit.Text = "Выход";
            this.ToolButton_Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 41);
            // 
            // ToolButton_Undo
            // 
            this.ToolButton_Undo.Enabled = false;
            this.ToolButton_Undo.Image = global::RogueAtNoir.Properties.Resources.undo_32x32;
            this.ToolButton_Undo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolButton_Undo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolButton_Undo.Name = "ToolButton_Undo";
            this.ToolButton_Undo.Size = new System.Drawing.Size(93, 38);
            this.ToolButton_Undo.Text = "Отменить";
            this.ToolButton_Undo.Click += new System.EventHandler(this.Undo_Click);
            // 
            // ToolButton_Redo
            // 
            this.ToolButton_Redo.Enabled = false;
            this.ToolButton_Redo.Image = global::RogueAtNoir.Properties.Resources.redo_32x32;
            this.ToolButton_Redo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolButton_Redo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolButton_Redo.Name = "ToolButton_Redo";
            this.ToolButton_Redo.Size = new System.Drawing.Size(113, 38);
            this.ToolButton_Redo.Text = "Восстановить";
            this.ToolButton_Redo.Click += new System.EventHandler(this.Redo_Click);
            // 
            // ToolButton_Rules
            // 
            this.ToolButton_Rules.Image = global::RogueAtNoir.Properties.Resources.help_browser_32x32;
            this.ToolButton_Rules.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolButton_Rules.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolButton_Rules.Name = "ToolButton_Rules";
            this.ToolButton_Rules.Size = new System.Drawing.Size(86, 38);
            this.ToolButton_Rules.Text = "Правила";
            this.ToolButton_Rules.Click += new System.EventHandler(this.Rules_Click);
            // 
            // GameTimeLabel
            // 
            this.GameTimeLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.GameTimeLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GameTimeLabel.Name = "GameTimeLabel";
            this.GameTimeLabel.Size = new System.Drawing.Size(117, 38);
            this.GameTimeLabel.Text = "Время: 00:00:00";
            this.GameTimeLabel.Visible = false;
            // 
            // CounterLabel
            // 
            this.CounterLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.CounterLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CounterLabel.Name = "CounterLabel";
            this.CounterLabel.Size = new System.Drawing.Size(56, 38);
            this.CounterLabel.Text = "Счёт: 0";
            this.CounterLabel.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.CounterLabel.Visible = false;
            // 
            // ClientArea
            // 
            this.ClientArea.AutoScroll = true;
            this.ClientArea.BackColor = System.Drawing.Color.DarkGreen;
            this.ClientArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientArea.Location = new System.Drawing.Point(0, 65);
            this.ClientArea.Name = "ClientArea";
            this.ClientArea.Size = new System.Drawing.Size(707, 393);
            this.ClientArea.TabIndex = 2;
            this.ClientArea.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.ClientArea.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ClientArea_MouseMove);
            this.ClientArea.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ClientArea_MouseDoubleClick);
            this.ClientArea.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ClientArea_MouseClick);
            this.ClientArea.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ClientArea_MouseDown);
            this.ClientArea.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ClientArea_MouseUp);
            // 
            // Context_Menu
            // 
            this.Context_Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CurSelecObject,
            this.Splitter1,
            this.NumberCards,
            this.Splitter2,
            this.InfoLabel});
            this.Context_Menu.Location = new System.Drawing.Point(0, 458);
            this.Context_Menu.Name = "Context_Menu";
            this.Context_Menu.Size = new System.Drawing.Size(724, 22);
            this.Context_Menu.TabIndex = 0;
            this.Context_Menu.Text = "statusStrip1";
            // 
            // CurSelecObject
            // 
            this.CurSelecObject.Name = "CurSelecObject";
            this.CurSelecObject.Size = new System.Drawing.Size(0, 17);
            // 
            // Splitter1
            // 
            this.Splitter1.Enabled = false;
            this.Splitter1.Name = "Splitter1";
            this.Splitter1.Size = new System.Drawing.Size(11, 17);
            this.Splitter1.Text = "|";
            // 
            // NumberCards
            // 
            this.NumberCards.Name = "NumberCards";
            this.NumberCards.Size = new System.Drawing.Size(0, 17);
            // 
            // Splitter2
            // 
            this.Splitter2.Enabled = false;
            this.Splitter2.Name = "Splitter2";
            this.Splitter2.Size = new System.Drawing.Size(11, 17);
            this.Splitter2.Text = "|";
            // 
            // InfoLabel
            // 
            this.InfoLabel.Name = "InfoLabel";
            this.InfoLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // ScrollBar
            // 
            this.ScrollBar.Dock = System.Windows.Forms.DockStyle.Right;
            this.ScrollBar.Location = new System.Drawing.Point(707, 65);
            this.ScrollBar.Name = "ScrollBar";
            this.ScrollBar.Size = new System.Drawing.Size(17, 393);
            this.ScrollBar.SmallChange = 10;
            this.ScrollBar.TabIndex = 3;
            this.ScrollBar.Visible = false;
            this.ScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.ScrollBar_Scroll);
            // 
            // GameTimer
            // 
            this.GameTimer.Interval = 1000;
            this.GameTimer.Tick += new System.EventHandler(this.GameTimer_Tick);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 41);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 41);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 480);
            this.Controls.Add(this.ClientArea);
            this.Controls.Add(this.ScrollBar);
            this.Controls.Add(this.Context_Menu);
            this.Controls.Add(this.MainToolStrip);
            this.Controls.Add(this.MainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.MainMenu;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Пасьянс. Красное и черное";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.MainToolStrip.ResumeLayout(false);
            this.MainToolStrip.PerformLayout();
            this.Context_Menu.ResumeLayout(false);
            this.Context_Menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_File;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_NewGame;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Exit;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Game;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Undo;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Redo;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Help;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Rules;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_AboutProgram;
        private System.Windows.Forms.ToolStrip MainToolStrip;

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ToolButton_NewGame;
        private System.Windows.Forms.ToolStripButton ToolButton_Exit;
        private System.Windows.Forms.ToolStripButton ToolButton_Undo;
        private System.Windows.Forms.ToolStripButton ToolButton_Redo;
        private System.Windows.Forms.ToolStripButton ToolButton_Rules;
        private System.Windows.Forms.Panel ClientArea;
        private System.Windows.Forms.StatusStrip Context_Menu;
        private System.Windows.Forms.ToolStripStatusLabel CurSelecObject;
        private System.Windows.Forms.ToolStripStatusLabel Splitter1;
        private System.Windows.Forms.ToolStripStatusLabel NumberCards;
        private System.Windows.Forms.ToolStripStatusLabel Splitter2;
        private System.Windows.Forms.ToolStripStatusLabel InfoLabel;
        private System.Windows.Forms.ToolStripLabel CounterLabel;
        private System.Windows.Forms.VScrollBar ScrollBar;
        private System.Windows.Forms.ToolStripLabel GameTimeLabel;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;

    }
}

